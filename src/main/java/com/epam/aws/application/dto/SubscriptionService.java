package com.epam.aws.application.dto;


import com.epam.aws.application.model.Subscription;
import com.epam.aws.application.repository.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@AllArgsConstructor
@Service
public class SubscriptionService {

    private final SubscriptionRepository repository;

    @Transactional
    public void save(String email, String subscriptionArn) {
        Subscription subscription = new Subscription();
        subscription.setSubscriptionArn(subscriptionArn);
        subscription.setEmail(email);
        repository.save(subscription);
    }

    @Transactional
    public String findSubscriptionArnByEmail(String email) {
        return repository.findByEmail(email)
                .map(Subscription::getSubscriptionArn)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Transactional
    public void deleteByEmail(String email) {
        repository.deleteByEmail(email);
    }
}
