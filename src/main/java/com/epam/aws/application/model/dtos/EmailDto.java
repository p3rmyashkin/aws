package com.epam.aws.application.model.dtos;

import lombok.Data;

@Data
public class EmailDto {
    private String email;
}
