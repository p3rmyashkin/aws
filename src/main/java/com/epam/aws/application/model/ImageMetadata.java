package com.epam.aws.application.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity(name = "image")
@Setter
@Getter
public class ImageMetadata {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;


    @Column(unique = true)
    private String name;

    @Column
    private String fileExtension;

    @Column
    private long size;

    @Column
    private Date lastUpdated;
}
