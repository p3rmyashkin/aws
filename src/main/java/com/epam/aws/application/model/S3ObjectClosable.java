package com.epam.aws.application.model;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.Closeable;
import java.io.IOException;

@AllArgsConstructor
public class S3ObjectClosable implements Closeable {

    private final S3Object s3Object;

    public S3ObjectInputStream getObjectContent() {
        return s3Object.getObjectContent();
    }

    @SneakyThrows
    public byte[] read() {
        return getObjectContent().readAllBytes();
    }

    @Override
    public void close() throws IOException {
        s3Object.getObjectContent().abort();
        s3Object.close();
    }
}
