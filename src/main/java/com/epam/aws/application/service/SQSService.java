package com.epam.aws.application.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.epam.aws.application.consts.AwsConstants;
import com.epam.aws.application.model.ImageMetadata;
import com.epam.aws.application.model.MessageBody;
import com.epam.aws.application.providers.ImageDownloadUrlProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class SQSService {

    private final AwsConstants awsConstants;
    private final AmazonSQS amazonSQS;
    private final SNSService snsService;
    private final ImageDownloadUrlProvider downloadUrlProvider;

    public void sendMessage(ImageMetadata imageMetadata) {
        String messageBody = MessageBody.builder()
                .name(imageMetadata.getName())
                .fileExtension(imageMetadata.getFileExtension())
                .size(imageMetadata.getSize())
                .lastUpdated(imageMetadata.getLastUpdated())
                .downloadUrl(downloadUrlProvider.getDownloadUrlForImage(imageMetadata))
                .build()
                .toString();
        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(awsConstants.getQueueUrl())
                .withMessageBody(messageBody);
        amazonSQS.sendMessage(sendMessageRequest);
        log.info("Message was send\n{}", messageBody);
    }


    //    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS) // Executes every 10 seconds
    public void readMessageAndSendToSnsAsBatch() {
        String queueUrl = awsConstants.getQueueUrl();
        List<Message> messages = amazonSQS.receiveMessage(queueUrl).getMessages();
        log.info("Number of messages read from queue '{}': {}", queueUrl, messages.size());
        if (messages.size() != 0) {
            snsService.sendBatch(messages);
            for (Message message : messages) {
                amazonSQS.deleteMessage(queueUrl, message.getReceiptHandle());
            }
        }
    }
}
