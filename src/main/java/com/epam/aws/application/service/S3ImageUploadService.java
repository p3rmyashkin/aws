package com.epam.aws.application.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.epam.aws.application.consts.AwsConstants;
import com.epam.aws.application.dto.ImageMetadataService;
import com.epam.aws.application.model.ContentFile;
import com.epam.aws.application.model.ImageMetadata;
import com.epam.aws.application.model.S3ObjectClosable;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@Service
@Slf4j
@AllArgsConstructor
public class S3ImageUploadService {

    private final AwsConstants awsConstants;
    private final AmazonS3 amazonS3;
    private final ImageMetadataService metadataService;
    private final SQSService sqsService;

    @SneakyThrows
    public void uploadImage(MultipartFile file) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(file.getSize());
        amazonS3.putObject(
                awsConstants.getBucketName(),
                resolveCompleteFilename(file.getOriginalFilename()),
                file.getInputStream(),
                objectMetadata
        );
        ImageMetadata metadata = metadataService.saveMetadata(file.getOriginalFilename(), file.getSize());
        sqsService.sendMessage(metadata);
    }

    public ContentFile getImageByName(String fileName) {
        try {
            String filepath = resolveCompleteFilename(fileName);
            String bucketName = awsConstants.getBucketName();
            if (amazonS3.doesObjectExist(bucketName, filepath)) {
                S3Object fileObject = amazonS3.getObject(bucketName, filepath);
                try (S3ObjectClosable s3ObjectClosable = new S3ObjectClosable(fileObject)) {
                    byte[] content = s3ObjectClosable.read();
                    return ContentFile.builder()
                            .filename(fileName)
                            .content(content)
                            .contentLength(content.length)
                            .build();
                } catch (IOException e) {
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            log.info("Unable to find image '{}' in the bucket '{}'", fileName, bucketName);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (AmazonS3Exception e) {
            log.warn("AmazonS3 exception occurred", e);
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    public void deleteFileByName(String fileName) {
        try {
            String filepath = resolveCompleteFilename(fileName);
            String bucketName = awsConstants.getBucketName();
            if (amazonS3.doesObjectExist(bucketName, filepath)) {
                amazonS3.deleteObject(bucketName, filepath);
                metadataService.deleteMetadata(fileName);
            }
        } catch (AmazonS3Exception e) {
            log.warn("AmazonS3 exception occurred", e);
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()));
        }
    }

    private String resolveCompleteFilename(String filename) {
        return awsConstants.getFolderName() + "/" + filename;
    }

    @PostConstruct
    public void createBucket() {
        String bucketName = awsConstants.getBucketName();
        if (!amazonS3.doesBucketExistV2(bucketName)) {
            amazonS3.createBucket(bucketName);
        }
    }
}
