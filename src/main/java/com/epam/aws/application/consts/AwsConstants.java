package com.epam.aws.application.consts;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class AwsConstants {

    public static final String AWS_ENDPONT = "aws/api/v1";
    public static final String IMAGE_ENDPOINT = AWS_ENDPONT + "/image";
    public static final String SNS_ENDPOINT = AWS_ENDPONT + "/sns";
    public static final String LAMBDA_ENDPOINT = AWS_ENDPONT + "/lambda";

    private final String S3AccessKey;
    private final String S3SecretKey;
    private final String adminAccessKey;
    private final String adminSecretKey;
    private final String EC2AccessKey;
    private final String EC2SecretKey;
    private final String S3Endpoint;
    private final String region;
    private final String bucketName;
    private final String folderName;
    private final String topicArn;
    private final String queueUrl;
    private final String lambdaName;

    public AwsConstants(@Value("${aws.s3.endpoint}") String S3Endpoint,
                        @Value("${aws.s3.accessKey}") String S3AccessKey,
                        @Value("${aws.s3.secretKey}") String S3SecretKey,
                        @Value("${aws.s3.bucketname}") String bucketName,
                        @Value("${aws.s3.folder}") String folderName,
                        @Value("${aws.ec2.accessKey}") String EC2AccessKey,
                        @Value("${aws.ec2.secretKey}") String EC2SecretKey,
                        @Value("${aws.admin.accessKey}") String adminAccessKey,
                        @Value("${aws.admin.secretKey}") String adminSecretKey,
                        @Value("${aws.sns.topicArn}") String topicArn,
                        @Value("${aws.sqs.queueUrl}") String queueUrl,
                        @Value("${aws.region}") String region,
                        @Value("${aws.lambda.function-name}") String functionName
    ) {
        this.S3AccessKey = S3AccessKey;
        this.S3SecretKey = S3SecretKey;
        this.adminAccessKey = adminAccessKey;
        this.adminSecretKey = adminSecretKey;
        this.EC2AccessKey = EC2AccessKey;
        this.EC2SecretKey = EC2SecretKey;
        this.S3Endpoint = S3Endpoint;
        this.region = region;
        this.bucketName = bucketName;
        this.folderName = folderName;
        this.topicArn = topicArn;
        this.queueUrl = queueUrl;
        this.lambdaName = functionName;
    }
}
