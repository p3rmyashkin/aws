package com.epam.aws.application.controller;

import com.epam.aws.application.consts.AwsConstants;
import com.epam.aws.application.model.dtos.EC2MetadataDto;
import com.epam.aws.application.providers.EC2MetadataProvider;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AwsConstants.AWS_ENDPONT)
@AllArgsConstructor
public class EC2AwsController {

    private final EC2MetadataProvider ec2MetadataProvider;

    @GetMapping("/metadata")
    public ResponseEntity<EC2MetadataDto> getEC2Metadata() {
        return ResponseEntity.ok(ec2MetadataProvider.provideMetadata());
    }
}
